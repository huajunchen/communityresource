var groupBy = function (xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
    return {
        date: this,
        string: "" + this.getFullYear() + "/" + (this.getMonth() + 1) + "/" + this.getDate()
    };
}
require([
    'dojo/dom', "dojo/on",

    "esri/WebMap",
    'esri/views/MapView',

    "esri/widgets/Search",

    "esri/tasks/support/Query",
    "esri/tasks/QueryTask",

    "dojo/query",
    "dojo/dom-construct",
    "dojo/dom-class",


    'dojo/domReady!'
], function (dom, on,
    WebMap, MapView,
    Search, Query, QueryTask,
    domQuery, domConstruct, domClass
) {
    'use strict';
    var mapViews = [];

    //display covid numbers
    number_dataSetting.layers.map(function (layer) {
        var fields = layer.querySettings.fields.sort(function (a, b) {
            return a.order - b.order
        });
        setQuery(layer.querySettings).then(function (results) {
            fields.forEach(function (field) {

                field.value = results.features[0].attributes[field.name];
                if (field.needFormat) {
                    field.value = field.needFormat(value);
                }

                var node0 = dom.byId("covid_data");
                var node = domConstruct.create("div", {
                    class: "covid-data-div box-div height-200"
                }, node0);
                var titleNode = domConstruct.create("div", {}, node);
                domConstruct.create("p", {
                    innerHTML: field.label,
                    class: "sub-title"
                }, titleNode);
                var valueNode = domConstruct.create("div", {}, node);
                domConstruct.create("p", {
                    innerHTML: field.value,
                    class: "covid-value covid-value-large"

                }, valueNode);

            })
        }).catch(function (e) {
            console.error(e);
        });
    });


    //display chart
    function setQuery(settings) {
        var query = new Query();
        var queryTask = new QueryTask({
            url: settings.queryUrl
        });
        if (settings.where) {
            query.where = settings.where;
        } else if (settings.getWhereCause) {
            query.where = settings.getWhereCause();
        }
        if (settings.orderByFields) {
            query.orderByFields = settings.orderByFields;
        } else {
            query.orderByFields = ["OBJECTID DESC"];
        }
        query.returnGeometry = false;

        if (settings.fields) {
            query.outFields = settings.fields.map(function (field) {
                return field.name
            });
        } else {
            query.outFields = ["*"];
        }
        return queryTask.execute(query);
    }

    function drawChart(dataArray, title, container) {

        google.charts.load('current', {
            'packages': ['bar']
        });
        google.charts.setOnLoadCallback(drawVisualization);

        function drawVisualization() {

            var options = {
                chart: {
                    title: title
                }
            };

            var chart = new google.charts.Bar(document.getElementById(container));
            chart.draw(google.visualization.arrayToDataTable(dataArray), google.charts.Bar.convertOptions(options));
        }

    }


    chart_dataSettings.layers.map(function (layer) {
        var fields = layer.querySettings.fields.sort(function (a, b) {
            return a.order - b.order
        });
        setQuery(layer.querySettings).then(function (results) {
            var dataArray = results.features.map(function (feature) {
                var values = fields.map(function (field) {
                    var value = feature.attributes[field.name];
                    if (field.needFormat) {
                        return field.needFormat(value);
                    }

                    return value;

                });
                return values;



            });
            dataArray.unshift(fields.map(function (field) {
                return field.label;
            }));

            drawChart(dataArray, layer.title, layer.container);

        }).catch(function (e) {
            console.error(e);
        });
    });

    //display maps
    webmap_setting.forEach(function (item) {
        var node0 = dom.byId("mapviews");

        var node = domConstruct.create("div", {
            id: item.id,
            class: "map-div box-div height-200"
        }, node0);

        var titleNode = domConstruct.create("div", {}, node);
        domConstruct.create("p", {
            innerHTML: item.title,
            class: "sub-title"
        }, titleNode);

        var mapNode = domConstruct.create("div", {
            id: item.id + "-mapview",
            class: "map-view",
            style: "width: 100%; height: 350px;"
        }, node);

        var buttonNode = domConstruct.create("div", {
            id: item.buttonId
        }, node);


        var webmap = new WebMap({
            portalItem: { // autocasts as new PortalItem()
                id: item.webmapId
            }
        });

        var view = new MapView({
            container: mapNode,
            map: webmap,
            zoom: default_setting.zoomlevel,
            center: default_setting.center,
            navigation: {
                gamepad: {
                    enabled: false
                },
                momentumEnabled: false,
                mouseWheelZoomEnabled: false
            }
        });
        setViewEvent(view);
        mapViews.push(view);
        view.when(function () {
            //when view ready, add event moniter on zoom in and out button
            addEventOnZoomButtons();
        }, function (error) {
            // This function will execute if the promise is rejected due to an error
            console.log(error)
        });
    });

    //update hyperlink buttons
    var buttonsByContainers = groupBy(hyperLinkButtons, 'container');
    Object.keys(buttonsByContainers).forEach(function (key) {
        var hyperLinkButtons = buttonsByContainers[key];
        hyperLinkButtons.sort(function (a, b) {
            return a.displayOrder - b.displayOrder;
        }).forEach(function (item) {
            var node0 = dom.byId(item.container);
            var node = domConstruct.create("div", {
                id: item.id,
                class: item.class ? item.class : ""
            }, node0);
            var linkNode = domConstruct.create("a", {
                href: item.url,
                class: "btn btn-primary",
                title: item.title,
                target: "_blank"
            }, node);
            if (item.image) {

                var imageNode = domConstruct.create("img", {
                    src: item.image,
                    border: "0",
                    alt: item.id,
                    width: "100%"
                }, linkNode);
                domClass.add(linkNode, "width100percent");

            }
            if (item.innerHtml) {
                linkNode.innerHTML = item.innerHtml;
            }
        });

    });


    //add search box
    var search = new Search({
        container: "search",
        view: mapViews[0]
    });

    // The following snippet uses the Search widget but can be applied to any
    // widgets that support the goToOverride property.
    search.goToOverride = function (view, goToParams) {
        return mapViews.forEach(function (view) {
            view.goTo(goToParams.target, goToParams.options)
        })
    };


    function setViewEvent(view) {
        view.on("double-click", function (event) {
            // the hitTest() checks to see if any graphics in the view
            // intersect the given screen x, y coordinates
            //add timeout to double click funtion
            setTimeout(function () {
                setExtents(mapViews, view);
            }, 500);
        });

        view.on("drag", function (event) {
            //Fires during a pointer drag on the view.

            if (event.action == "end") {
                //when drag finished
                setExtents(mapViews, view);
            }

        });

        view.on("hold", function (event) {
            // Fires after holding either a mouse button or a single finger on the view for a short amount of time.
            //open big map in a iframe


        });

    }
    //set moniter on zoom in and out ui button function
    function addEventOnZoomButtons() {
        domQuery(".esri-icon-plus,.esri-icon-minus").forEach(function (item) {
            var button = item.parentNode;
            var currentViewDiv = button.closest(".map-view");
            var currentView = mapViews.find(function (view) {
                return view.container == currentViewDiv;
            });
            if (currentView) {
                on(button, "click", function () {
                    setTimeout(function () {
                        setExtents(mapViews, currentView);
                    }, 500);
                });

            }
        })
    }

    function setExtents(mapViews, currentView) {
        mapViews.forEach(function (view) {
            if (view != currentView) {
                view.goTo({
                    target: currentView.extent
                });
            }
        })



    }


});