default_setting = {
  "basemap": "streets", //streets, topo
  "zoomlevel": 14,
  "center": [-96.636269, 32.91676]
}
var number_dataSetting = {
  layers: [{
      id: "covid-confirmed-cases",
      title: "Confirmed Cases",
      container:"covid_data",
      querySettings:{
        queryUrl: "https://services2.arcgis.com/g3rbttPStUJTjAz2/ArcGIS/rest/services/survey123_0a319590ccd34acd9d8e9474d737333d/FeatureServer/0",
        fields:[{
          name: "confirmed_cases",
          label: "Confirmed Cases",
          order: 0
        },{
          name: "monitored_cases",
          label: "Monitored Cases",
          order: 1
        }

        ],
        orderByFields:["date_reported DESC"],
        where:"confirmed_cases > 0 and  monitored_cases > 0"
      }
      
    }    
  ]
}


var chart_dataSettings = {
  layers: [{
    id: "chart-911",
    title: "911 Calls possibly COVID-19",
    container: "chart-911",
    querySettings: {
      queryUrl: "https://services2.arcgis.com/g3rbttPStUJTjAz2/ArcGIS/rest/services/survey123_67a3446b0fc34fdfa25508c7fb074d1b/FeatureServer/0",
      fields: [{
          name: "calls_reported",
          label: "Calls",
          order: 1
        },
        {
          name: "reported_date",
          label: "Reported",
          order: 0,
          needFormat: function (val) {
            var dateVal = new Date(val);
            return "" + (dateVal.getMonth() + 1) + "/" + dateVal.getDate();
          }
        }
      ],
      orderByFields: ["reported_date"],
      getWhereCause: function () {
        var from = new Date().addDays(-7);
        return "calls_reported is not null and reported_date > date'" + from.string + "'";
      }
    }

  }]

}


var webmap_setting = [{
    id: "restaurantMap",
    title: "Restaurants Offering Delivery and Take Out",
    webmapId: "ed0226b2657d4caab848baf5b948b893",
    buttonId: "btn-restaurant-map"
  },
  {
    id: "gisdMap",
    title: "Schools offering free meals",
    webmapId: "5f6036bc37f94bcf87ea5986457e90cc",
    zoomlevel: 14,
    "center": [-96.636269, 32.91676],
    buttonId: "btn-school-map"
  }

]

var hyperLinkButtons = [{
    id: "btn-city-facility-map",
    displayOrder: 0,
    container: "image-hyperlink-buttons",
    class: "box-div",
    image: "images/cityFacilityButton.png",
    url: "https://maps.garlandtx.gov/cogmap/apps/webappviewer/index.html?id=a3e02d0651504c18986fb40eaae6b4a2",
    title: "Open to see city facilities that are on duty"

  }, {
    id: "btn-gisd-meal-map",
    displayOrder: 1,
    container: "image-hyperlink-buttons",
    class: "box-div",
    image: "images/gisdMapButton.png",
    url: "https://www.garlandisd.net/node/11272",
    title: "Open to see Curbside meal pickup locations on GISD.com"

  },
  {
    id: "btn-school-map",
    displayOrder: 0,

    container: "btn-school-map",
    innerHtml: "Open to see full map",
    url: "https://garland.maps.arcgis.com/apps/webappviewer/index.html?id=27af0cc674384a8f9d26408fcd9a35c4",
    title: "Open to see schools that are offering free meal"
  },
  {
    id: "btn-restaurant-map",
    displayOrder: 0,
    container: "btn-restaurant-map",
    innerHtml: "Open to see full map",
    url: "https://garland.maps.arcgis.com/apps/webappviewer/index.html?id=25697a63212549ac9dfb2a03b53ac966",
    title: "Open to see restaurants that does take out and delivery"

  }


]